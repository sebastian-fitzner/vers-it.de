import { Veams } from './app.veams';
import Particles from './shared/components/particles/particles';
import CTA from './shared/components/cta/scripts/cta';
import Navigation from './core/components/navigation/scripts/navigation';

// Initialize modules with Veams

Veams.modules.add({
	namespace: 'particles',
	module: Particles
});

Veams.modules.add({
	namespace: 'cta',
	module: CTA
});

Veams.modules.add('navigation', Navigation);
// @INSERTPOINT :: @ref: js-init-v5, @keep: true //
// @INSERTPOINT :: @ref: js-init-once-v5, @keep: true //