Jan-Niklas Keese<br>
**Geschäftsführer**

- fon: +49 (30) 809 334 210
- funk: +49 (151) 585 114 18
- fax: +49 (30) 809 334 299
- mail: j.keese@vers-it.de
- www: vers-it.net

Kurfürstenstraße 47 • 13467 Berlin