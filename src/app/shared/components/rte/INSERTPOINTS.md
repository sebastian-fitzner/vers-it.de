## Usage

### Include: Page

``` hbs
// @INSERT :: START @id: rte, @tag: component-partial }}
{{#with rte-bp.variations.default}}
	{{> c-rte}}
{{/with}}
{{! @INSERT :: END }}
```