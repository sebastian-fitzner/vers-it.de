# iconTeaser (`component`)

## Description

This blueprint is based on the blueprint of Veams.

> Just place a generic quote for your type.

-----------

## Requirements
- `Veams#5.0.0` - Replace this requirement with the libs you need for this type.

-----------

## Installation

### Installation with Veams from local machine

`veams install bp absolute/filepath/to/icon-teaser`

### Installation with Bower or Veams

When published on bower you can install the component by executing:

1. `veams install veams-component icon-teaser`
2. `bower install veams-component-icon-teaser --save`

-----------

## Fields

### `c-icon-teaser.hbs`

#### Settings
- settings.iconTeaserClasses {`String`} - _Modifier classes for component._
- settings.iconTeaserContextClass {`String`} [default] - _Context class of component._ 

#### Content
- content.iconTeaserField {`String`} - _Please add a description!_

------------

## SASS

### Variables

- $icon-teaser-my-custom-var {`String`} [] - _Please add a description!_

### Modifier Classes

You can add the following modifiers to `icon-teaser`:
- is-modifier - _Please add a description!_
