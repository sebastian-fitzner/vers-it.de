## INSERTPOINTS

### Include: Page

``` hbs
{{! @INSERT :: START @id: particles, @tag: component-partial }}
{{#with particles-bp}}
	{{> c-particles}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: JavaScript

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init Particles
 */
Helpers.loadModule({
	el: '[data-js-module="particles"]',
	module: Particles,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init Particles
 */
Helpers.loadModule({
	domName: 'particles',
	module: Particles,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V5
``` js
// @INSERT :: START @tag: js-init-v5 //
	,
	{
// Init Particles
		namespace: 'particles',
		module: Particles
	}
	// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for Particles
 */
EVENTS.particles = {
	eventName: 'particles:eventName'
};
// @INSERT :: END
```
