## INSERTPOINTS

### Include: Page

``` hbs
{{! @INSERT :: START @id: service-plan, @tag: component-partial }}
{{#with service-plan-bp}}
	{{> c-service-plan}}
{{/with}}
{{! @INSERT :: END }}
```
