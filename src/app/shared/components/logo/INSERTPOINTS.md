## INSERTPOINTS

### Include: Page

``` hbs
{{! @INSERT :: START @id: logo, @tag: component-partial }}
{{#with logo-bp}}
	{{> c-logo}}
{{/with}}
{{! @INSERT :: END }}
```
