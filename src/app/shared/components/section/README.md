# section (`component`)

## Description

This blueprint is based on the blueprint of Veams.

> Just place a generic quote for your type.

-----------

## Requirements
- `Veams#5.0.0` - Replace this requirement with the libs you need for this type.

-----------

## Installation

### Installation with Veams from local machine

`veams install bp absolute/filepath/to/section`

### Installation with Bower or Veams

When published on bower you can install the component by executing:

1. `veams install veams-component section`
2. `bower install veams-component-section --save`

-----------

## Fields

### `c-section.hbs`

The partial is a `wrapWith` partial.

#### Settings
- settings.sectionClasses {`String`} - _Modifier classes for component._
- settings.sectionContextClass {`String`} [default] - _Context class of component._ 

#### Content
- content.sectionField {`String`} - _Please add a description!_

------------

## SASS

### Variables

- $section-my-custom-var {`String`} [] - _Please add a description!_

### Modifier Classes

You can add the following modifiers to `section`:
- is-modifier - _Please add a description!_
