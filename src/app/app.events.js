/**
 * Const for events (pub/sub)
 *
 * @author: Sebastian Fitzner
 */

/**
 * Events Global
 */
const EVENTS = {};


/**
 * Events for Navigation
 */
EVENTS.navigation = {
	toggle: 'navigation:toggle'
};
// @INSERTPOINT :: @ref: js-events

export default EVENTS;