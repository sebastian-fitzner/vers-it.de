/**
 * Description of Navigation.
 *
 * @module Navigation
 * @version v0.0.0
 *
 * @author your_name
 */

// Imports
import { Veams } from 'app.veams';
import VeamsComponent from 'veams/src/js/common/component'; // Only use that in combination with browserify
// import VeamsComponent from 'veams/lib/common/component'; // Can be used in general

// Variables
const $ = Veams.$;
const Helpers = Veams.helpers;

class Navigation extends VeamsComponent {
	/**
	 * Constructor for our class
	 *
	 * @see module.js
	 *
	 * @param {Object} obj - Object which is passed to our class
	 * @param {Object} obj.el - element which will be saved in this.el
	 * @param {Object} obj.options - options which will be passed in as JSON object
	 */
	constructor(obj) {
		let options = {
			activeClass: 'is-active',
			scrollClass: 'isnt-scrollable',
			navItems: '[data-js-item="nav-item"]'
		};

		super(obj, options);
	}

	/**
	 * Get module information
	 */
	static get info() {
		return {
			version: '0.0.0'
		};
	}

	/**
	* Event handling
	*/
	get events() {
		return {
			// 'click': 'render'
		};
	}

	/**
	* Subscribe handling
	*/
	get subscribe() {
		return {
			'{{Veams.EVENTS.navigation.toggle}}': 'toggleNavigation'
		};
	}

	/**
	 * Initialize the view
	 *
	 */
	initialize() {
		console.log('init Navigation');
	}

	/**
	 * Render class
	 */
	render() {
		return this;
	}

	/**
	 * Toggle Navigation
	 */
	toggleNavigation(e) {
		if (this.$el.hasClass(this.options.activeClass)) {
			this.$el.removeClass(this.options.activeClass);
			$('body').removeClass(this.options.scrollClass);
		} else {
			this.$el.addClass(this.options.activeClass);
			$('body').addClass(this.options.scrollClass);
		}
	}
}

export default Navigation;
