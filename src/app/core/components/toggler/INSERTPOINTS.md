
## INSERTPOINTS

### Include: Page

``` hbs
{{! @INSERT :: START @id: toggler, @tag: component-partial }}
{{#with toggler}}
	{{> toggler}}
{{/with}}
{{! @INSERT :: END }}
```
