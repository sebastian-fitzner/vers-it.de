
# toggler (`component`)

## Description

This blueprint is based on the blueprint of Veams.

> Just place a generic quote for your type.

-----------

## Requirements
- `Veams#5.0.0` - Replace this requirement with the libs you need for this type.

-----------

## Installation

### Installation with Veams from local machine

`veams install bp absolute/filepath/to/toggler`

### Installation with npm or Veams

When published on npm you can install the component by executing:

1. `veams install veams-component toggler`
2. `bower install veams-component-toggler --save`

-----------

## Fields

### `c-toggler`

#### Settings
- settings.togglerClasses {`String`} - _Modifier classes for component._
- settings.togglerContextClass {`String`} [default] - _Context class of component._ 

#### Content
- content.togglerField {`String`} - _Please add a description!_

------------

## SASS

### Variables

- $toggler-my-custom-var {`String`} [] - _Please add a description!_

### Modifier Classes

You can add the following modifiers to `toggler`:
- is-modifier - _Please add a description!_
