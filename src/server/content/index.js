import { Router } from 'express';

// // Routes
import index from './home';
import serverHosts from './server-hosts';

const router = new Router();

// Basic routes
router.get([ '/', '/home', '/index' ], index);
router.get('/*/', (req, res, next) => {
	if (req.url.includes('.html')) {
		next();
	} else {
		res.redirect(`/${req.params['0']}index.html`);
	}
});

// Additional routes
router.use([ serverHosts ]);


module.exports = router;